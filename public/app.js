
fetch('/api/amounts')
.then(function(res) {
  return res.json()
}) 
.then(function(res) {
  return res.map(function(value,i) {
    return {
      name: value.toLocaleString(),
      value,
      selected: i == 0,
      icon: 'newspaper outline'
    }
  })
})
.then(function(values) {
  $('.ui.dropdown').dropdown({
    values,
  });
});

$('#overnight_delivery input').change(function() {
  this.checked
    ? $(this).next().html('ja')
    : $(this).next().html('nein')
});

function promptAndSubmit(event, fields) {
  const { amount, name, street, houseNumber, zip, city, email } = fields
  $('body').toast({
    message: `Möchten Sie <b>${amount} Exemplare</b> der aktuellen Ausgabe »Demokratischer Widerstand« an folgende Adresse bestellen und verteilen?<br>\
    <br><i><b>${name}</b><br>${street} ${houseNumber}<br>${zip} ${city}</i><br>\
    <br><b>Wichtiger Hinweis:</b> Sie erhalten nun eine automatische E-Mail. Ihre Bestellung wird erst entgegen genommen, wenn Sie den <b>Link</b> in dieser E-Mail klicken. Überprüfen Sie bitte auch Ihren <b>Spamordner!</b>`,
    displayTime: 0,
    class: 'white',
    classActions: 'bottom attached',
    actions:	[{
      icon: 'ban',
      class: 'icon grey',
      text: ' doch nicht!'
    },{
      text: 'Jippie!',
      class: 'pink',
      click: function() {
        console.log(fields)
        fetch('/api/submit', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(fields),
        })
        .then(response => response.json())
        .then(data => {
          console.log(data);
          $('body').toast({
            message: `Sie müssen nun noch den Link in der E-Mail klicken, welche wir Ihnen an <b>${email}</b> senden.`,
            displayTime: 6000,
            class: 'inverted yellow',
          })
        })
        .catch((error) => {
          console.error(error);
        });
      }
    }]
  })
;
}

$('document').ready(function() {
  $('.ui.form').form({
    on: 'blur',
    fields: {
      name:           'empty',
      street:         'empty',
      houseNumber:    'empty',
      zip:            ['empty', 'integer'],
      city:           'empty',
      phone:          ['empty','number'],
      email:          ['empty','email'],
      privacy_notice: 'checked',
      overnight_delivery: 'checked'
    },
    onSuccess: promptAndSubmit
  });
});

$('.ui.button.submit').click(function() {
  $('.ui.form').form('submit')
})