const order = {
  "id": "order",
  "type": "object",
  "properties": {
    amount: {type: "int"},
    city: {type: "string"},
    email: {type: "string"},
    houseNumber: {type: "string"},
    name: {type: "string"},
    overnight_delivery: {type: "string"},
    phone: {type: "string"},
    street: {type: "string"},
    zip: {type: "int"},
  },
  "required": ["amount", "city", "email", "houseNumber", "name", "overnight_delivery", "phone", "privacy_notice", "street", "zip"]
}

module.exports = {
  order
}