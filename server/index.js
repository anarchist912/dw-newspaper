const IS_PROD = process.env.NODE_ENV == 'production'

const path = require('path');
const reload = IS_PROD ? () => {} : require('reload');
const gaze = IS_PROD ? () => {} : require('gaze');
const { PORT = 8000 } = process.env;
const express = require('express');
const json = require('body-parser').json;
const app = express();;
const { create, validateMail } = require('./controller');

const validate = require('jsonschema').validate;
const schema = require('./schema');

const { sendOptIn } = require('./mail');

const ejs = require('ejs');

const Ddos = require('ddos');
const ddos = new Ddos({burst:19, limit:20})

app.use(ddos.express);
app.use(require('helmet')());

app.use(express.static('public'));
app.use(express.static('semantic/dist'));


app.get('/', (req, res) => {
  res.sendFile(path.resolve('public/index.html'));
});

app.get('/api/amounts', (req, res) => {
  const amounts = [3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000, 15000, 20000]
  res.send(amounts)
});

app.post('/api/submit', json(), (req, res) => {
  const order = validate(req.body, schema.order)
  if (order.valid) {
    req.body['user_agent'] = req.headers['user-agent'];
    req.body['ip'] = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    create(req.body, (err, result) => {
      if (!err) {
        res.send({success: true})
        sendOptIn(req.body, result['token'])
      } else
        res.send({success: false, err})
    })
  } else {
    res.send({success: false})
  }
});

app.get('/api/validate/:token', (req, res) => {
  const { token } = req.params
  validateMail(token, (err, result) => {
    if (!err) {
      const validated = result.modifiedCount == 1
      const data = {
        message: validated
            ? "Bestellung erfolgreich bestätigt. Vielen Dank! Die gewünschte Menge wird geliefert solange der Vorrat reicht."
            : "Entweder diese Bestellung wurde bereits bestätigt oder der Link ist ungültig.",
        messageType: validated ? 'green' : 'yellow',
        messageIcon: validated ? "check icon" : "question icon",
        messageTitle: validated ? "Bestellung bestätigt" : "Hmmm.."
      }
      ejs.renderFile(path.resolve('public/validate.html'), data, {}, (err, str) => {
        res.send(str)
      })
    } else {
      const data = {
        message: "Es ist ein unbekannter Fehler aufgetreten. Bitte informieren Sie den Administrator unter <a href=\"mailto:zeitung-austragen@protonmail.com\">zeitung-austragen@protonmail.com</a>.",
        messageType: "red",
        messageIcon: "times icon",
        messageTitle: "Unbekannter Fehler"
      }
      ejs.renderFile(path.resolve('public/validate.html'), data, {}, (err, str) => {
        res.send(str)
      })
    }
  })
})

if (IS_PROD) {
  app.listen(PORT, () => console.log("[dw-newspaper] Running at Port " + PORT));
} else {
  reload(app)
  .then(reloadReturned => {
      gaze('public/index.html', (err, watcher) => {
        watcher.on('changed', filepath => reloadReturned.reload());
      });
    app.listen(PORT, () => console.log("[dw-newspaper] Running at Port " + PORT));
  })
  .catch(function (err) {
    console.error('[reload]', err)
  });
}
