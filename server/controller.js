const db = require('./mongo').getDb();
const { ObjectId } = require('mongodb');

var cl;
db.then(db => {
  cl = db.collection('orders');
})




const create = (order, callback) => {
  order["optin"] = false
  order["createdAt"] = new Date()
  order["lastModified"] = new Date()
  require('crypto').randomBytes(48, function(err, buffer) {
    order["token"] = buffer.toString('hex');
    cl.insertOne(order, (err, result) => {
      if (!err)
        result['token'] = order['token']
      callback(err, result);
    })
  });
}

const validateMail = (token, callback) => {
  cl.updateOne({token}, {
    $set: { optin: true, lastModified: new Date() }
  }, (err, result) => {
    callback(err, result);
  })
}

module.exports = {
  create,
  validateMail
}