const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(process.env.SENDGRID_API_KEY);

const sendOptIn = ({email, name, street, houseNumber, zip, city, amount, overnight_delivery}, token) => {
  const msg = {
    to: email,
    from: 'no-reply@nichtohneuns.de',
    replyTo: 'zeitung-austragen@protonmail.com',
    subject: 'Bitte bestätigen Sie Ihre Bestellung »Demokratischer Widerstand«',
    text: `Sehr geehrte/r MitstreiterIn,\n\ndamit Ihre Bestellung berücksichtigt wird, bitte bestätigen Sie noch Ihre E-Mail-Adresse durch Aufruf folgenden Links:\n\nhttps://zeitung.nichtohneuns.net/api/validate/${token}\n\n${name}\n${street} ${houseNumber}\n${zip} ${city}\n\nAbstellung nachts vor Tür: ${overnight_delivery ? 'ja' : 'nein'}\n\n\nImpressum: https://nichtohneuns.de/impressum\nDatendschutz: https://nichtohneuns.de/datenschutz`,
    html: `Sehr geehrte/r MitstreiterIn,<br><br>damit Ihre Bestellung berücksichtigt wird, bitte bestätigen Sie noch Ihre E-Mail-Adresse durch Aufruf folgenden Links:<br><br><a href="https://zeitung.nichtohneuns.net/api/validate/${token}">https://zeitung.nichtohneuns.net/api/validate/${token}</a><br><br><i>${name}<br>${street} ${houseNumber}<br>${zip} ${city}</i><br><br>Abstellung nachts vor Tür: ${overnight_delivery ? 'ja' : 'nein'}<br><br><br><a href="https://nichtohneuns.de/impressum">Impressum</a> | <a href="https://nichtohneuns.de/datenschutz">Datenschutz</a>`,
  };
  //ES6
  sgMail
  .send(msg)
  .then(() => {}, error => {
    console.error(error);

    if (error.response) {
      console.error(error.response.body)
    }
  });
}

module.exports = {
  sendOptIn
}