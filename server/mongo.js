const IS_PROD = process.env.NODE_ENV == 'production'

const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

// Client and db
var mongoClient;
var db;
const options = { useUnifiedTopology: true }

// Connection URL
const url = IS_PROD
  ? `<>`
  : 'mongodb://localhost:27017'
  
// Database Name
const dbName = 'dw_newspaper_db';
 
// Use connect method to connect to the server
MongoClient.connect(url, options, function(err, client) {
  assert.equal(null, err);
  console.log(`Connected successfully to MongoDB at ${url.replace('mongodb://', '')}`);

  mongoClient = client;
  db = client.db(dbName);
  console.log(`Database ${dbName} ready:`, db != undefined)
});

const getClient = () => mongoClient
const getDb = () => new Promise((resolve, reject) => {
  const maxRetries = [0, 30]
  const retry = setInterval(() => {
    if (db != undefined) {
      resolve(db);
      clearInterval(retry);
    } else {
      maxRetries[0] += 1
      if (maxRetries[0] == maxRetries[1])
        reject("timed out")
    }
  }, 100);
});


module.exports = {
  getClient,
  getDb
}